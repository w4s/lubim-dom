jQuery(document).ready(function($) {

	//OWL-HOME CAROUSEL
	$('.owl-home').owlCarousel({
		loop:true,
		nav:true,
		items: 1,
		navContainer: '#owl-home-navigation',
		navText: ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"]
	});

	//OWL MANUFACTURER
	$('.owl-manufacturer').owlCarousel({
		loop:true,
		nav:true,
		margin: 40,
		autoWidth:true,
		responsive:{
			0:{
				items:3
			},
			600:{
				items:5
			},
			1000:{
				items:8
			}
		},
		navContainer: '#owl-manufacturer-navigation',
		navText: ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"]
	});

	//OWL MANUFACTURER
	$('.owl-category-manufacturer').owlCarousel({
		loop:true,
		nav:true,
		margin: 45,
		autoWidth:true,
		responsive:{
			0:{
				items:2
			},
			600:{
				items:3
			},
			1000:{
				items:4
			}
		},
		navContainer: '#owl-manufacturer-navigation',
		navText: ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"]
	});

	//OWL RELATED PRODUCTS
	$('.owl-related-products').owlCarousel({
		loop:true,
		nav:true,
		margin: 45,
		responsive:{
			0:{
				items:1
			},
			600:{
				items:2
			},
			1000:{
				items:4
			}
		},
		navContainer: '#owl-related-navigation',
		navText: ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"]
	});

	//OWL PRODUCT GALLERY
	$('.owl-product-gallery').owlCarousel({
		items:1,
		loop:true,
		dotsData:true,
		nav:true,
		navText: ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"]
	});

	// CATALOGUE NAVIGATION
	$('.catalogue').click(function(){
		$( this ).toggleClass('active')
		$('#nav-overley').toggle();
		$('#catalogue-menu').toggle('slow');
	});
	$('#nav-overley').click(function(){
		$('.catalogue').toggleClass('active')
		$( this ).toggle();
		$('#catalogue-menu').toggle('slow');
	});
	$('.catalogue-menu > li > a').hover(function() {
		$( '#catalogue-menu' ).removeClass('catalogue-menu-full-width');
		$( '.catalogue-menu > .menu-item-has-children > a' ).removeClass('active');
		$( '.catalogue-menu > .menu-item-has-children > a' ).next('.sub-menu').removeClass('hovered');
	});
	$('.catalogue-menu > .menu-item-has-children > a').hover(function() {
		$( this ).addClass('active');
		$( this ).next('.sub-menu').addClass('hovered');
		$( '#catalogue-menu' ).addClass('catalogue-menu-full-width');
	});

	//Кнопка "Наверх"
	//Документация:
	//http://api.jquery.com/scrolltop/
	//http://api.jquery.com/animate/
	$(window).scroll(function () {
		if ($(this).scrollTop() > 0) {
			$('#top').fadeIn();
		} else {
			$('#top').fadeOut();
		}
	});
	$("#top").click(function () {
		$("body, html").animate({
			scrollTop: 0
		}, 800);
		return false;
	});

	//Initialization Fancybox
	$('[data-fancybox]').fancybox({
		animationEffect : 'fade'
	});
	
	//Initialization Fancybox to gallery
	$('.gallery-item a').attr( "data-fancybox", "Галерея" );

	//E-mail Ajax Send
	$('.default-form').submit(function (e) {
		var formData = new FormData($(this)[0]);
		$.ajax({
			url: '/wp-content/themes/lubim-dom/mail.php',
			type: 'POST',
			data: formData,
			async: false,
			success: function (msg) {
				$.fancybox.close(); //Закрыть pop-up
				setTimeout(function() {
					alert(msg);
				}, 1000);
			},
			error: function (msg) {
				alert('Ошибка!');
			},
			cache: false,
			contentType: false,
			processData: false
		});
		e.preventDefault();
	});

	//Initialization Maskedinput
	$('.phone-input').mask('+7 (999) 999-9999');

	//Mobile header menu
	$('.toggle-nav').click(function() {
		$('.header-mobile-menu').toggle('slow');
		$('.user-profile > .icon-mobile').toggle();
		$('.user-profile > .registration').toggle();
	});	
	$('.header-mobile-menu .menu-item-has-children > a').click(function() {
		$(this).parent().toggleClass('active');
		$(this).next().toggle('slow');
		return false;
	});

	//Mobile catalogue navigation
	$('.mobile-catalogue-menu .menu-item-has-children > a').click(function() {
		$(this).parent().toggleClass('active');
		$(this).next().toggle('slow');
		return false;
	});

	//Open mobile search
	$('.open-search-form').click(function() {
		$(this).toggleClass('active');
		$('.main-header .bottom-bar .search-block').toggle('slow');
	});

	//Open basket
	$('.basket-block > .number').hover(function() {
		$( '.wrap-product-basket' ).fadeIn('slow');
	});
	$('.basket-block > h3').hover(function() {
		$( '.wrap-product-basket' ).fadeIn('slow');
	});

	//Close basket
	$('.close-basket').click(function() {
		$( '.wrap-product-basket' ).hide();
	});

	//Cart
	if ( $.cookie('quantity') ) {
		var quantity = $.cookie('quantity');
		$('#no-products').remove();
	} else {
		var quantity = 0;
		$.cookie('quantity', '0', {
			path: '/',
		});
	}

	$('.quantity').text( $.cookie('quantity') );
	$('.product-basket').append( $.cookie('products') );

	//Add to cart
	$('.add-to-cart').click(function() {
		//Количество 
		quantity = $.cookie('quantity');
		quantity++;
		$.cookie('quantity', quantity, {
			path: '/',
		});
		$('.quantity').text( $.cookie('quantity') );

		//Товары
		$('#no-products').remove();
		if ( $(this).parent().hasClass('product-item') ) {
			var product = $(this).parent().children('.title');
		} else {
			var product = $('.entry-title');
		}
		$('.product-basket').append( '<li><a href="'+ product.attr('href') +'">' + product.text() + '</a><i class="fa fa-minus-circle" onclick="deleteProductCart(this)"></i></li>' );
		$.cookie('products', $('.product-basket').html(), {
			path: '/',
		});

		$.fancybox.open('<div class="modal-add-to-cart"><div class="aligncenter"><h2>'+ product.text() +'</h2><p>Добавлен в корзину <i class="fa fa-check-circle" style="color: green;"></i></p></div></div>');
		//Закрыть pop-up
		setTimeout("jQuery.fancybox.close();", 1500);
	});

	//Delete cart
	$('#clear-basket').click(function() {
		quantity = 0;
		$.cookie('quantity', quantity, {
			path: '/',
		});
		$('.quantity').text( $.cookie('quantity') );
		$.cookie('quantity', '0', {
			path: '/',
		});
		$('.product-basket').html('<li id="no-products">Нет товаров</li>');
		$.cookie('products', $('.product-basket').html(), {
			path: '/',
		});
	});

	//Для оформления заказа
	$('.open-checkout-modal').click(function() {
		var li = $('.product-basket li');
		var vals = li.map(function(){
			return $(this).text();
		}).get();
		$('#ordering-product-list').val(vals);
		// console.log(vals);
	});

	//Filters
	var paramsString = location.search;
	var searchParams = new URLSearchParams(paramsString);

	//Формат
	$('#search-format').on('change', 'input[type="checkbox"]', function(){

		var sortValues = $(this).val(); //получаем значение выбранного пункта select

		var pathArray = window.location.pathname.split('.html');
		var secondLevelLocation = pathArray[0];

		if( !($(this).prop( "checked" )) ) { 

			searchParams.delete("format");
			window.history.replaceState({}, '', secondLevelLocation + '.html?' + searchParams);
			location.reload(); //перезагрузить старницу (для ajax не нужно)

		} else {

			searchParams.set("format", sortValues);
			window.history.replaceState({}, '', secondLevelLocation + '.html?' + searchParams);
			location.reload(); //перезагрузить страницу (для ajax не нужно)

		}

	});

	//Производитель
	$('#search-manufacturer').on('change', 'input[type="checkbox"]', function(){

		var sortValues = $(this).val(); //получаем значение выбранного пункта select

		var pathArray = window.location.pathname.split('.html');
		var secondLevelLocation = pathArray[0];

		if( !($(this).prop( "checked" )) ) {

			searchParams.delete("manufacturer");
			window.history.replaceState({}, '', secondLevelLocation + '.html?' + searchParams);
			location.reload(); //перезагрузить старницу (для ajax не нужно)
				
		} else {
				
			searchParams.set("manufacturer", sortValues);
			window.history.replaceState({}, '', secondLevelLocation + '.html?' + searchParams);
			location.reload(); //перезагрузить страницу (для ajax не нужно)
				
		}

	});

	//Марка прочности
	$('#search-strength_grade').on('change', 'input[type="checkbox"]', function(){

		var sortValues = $(this).val(); //получаем значение выбранного пункта select

		var pathArray = window.location.pathname.split('.html');
		var secondLevelLocation = pathArray[0];

		if( !($(this).prop( "checked" )) ) {

			searchParams.delete("strength_grade");
			window.history.replaceState({}, '', secondLevelLocation + '.html?' + searchParams);
			location.reload(); //перезагрузить старницу (для ajax не нужно)
				
		} else {
				
			searchParams.set("strength_grade", sortValues);
			window.history.replaceState({}, '', secondLevelLocation + '.html?' + searchParams);
			location.reload(); //перезагрузить страницу (для ajax не нужно)
				
		}

	});

	//Марка прочности
	$('#search-color').on('change', 'input[type="checkbox"]', function(){

		var sortValues = $(this).val(); //получаем значение выбранного пункта select

		var pathArray = window.location.pathname.split('.html');
		var secondLevelLocation = pathArray[0];

		if( !($(this).prop( "checked" )) ) {

			searchParams.delete("color");
			window.history.replaceState({}, '', secondLevelLocation + '.html?' + searchParams);
			location.reload(); //перезагрузить старницу (для ajax не нужно)
				
		} else {
				
			searchParams.set("color", sortValues);
			window.history.replaceState({}, '', secondLevelLocation + '.html?' + searchParams);
			location.reload(); //перезагрузить страницу (для ajax не нужно)
				
		}

	});
	//End filters

	//Orderby
	$('#orderby').change(function(){

		var sortValues = $("#orderby option:selected").val(); //получаем значение выбранного пункта orderby

		if($("#orderby").val()=="") { 
				
			searchParams.delete("orderby");
			window.history.replaceState({}, '', location.pathname + '?' + searchParams);
			location.reload(); //перезагрузить старницу (для ajax не нужно)
				
		} else {
				
			searchParams.set("orderby", sortValues);
			window.history.replaceState({}, '', location.pathname + '?' + searchParams);
			location.reload(); //перезагрузить страницу (для ajax не нужно)
			
		}

	});
	//END orderby
	//Сортировка Количество записей

	$('#number').change(function(){

		var sortValues = $("#number option:selected").val(); //получаем значение выбранного пункта select
		//alert( sortValues );

		if($("#number").val()=="") { 
				
			searchParams.delete("posts_per_page");
			window.history.replaceState({}, '', location.pathname + '?' + searchParams);
			location.reload(); //перезагрузить старницу (для ajax не нужно)
				
		} else {
				
			searchParams.set("posts_per_page", sortValues);
			window.history.replaceState({}, '', location.pathname + '?' + searchParams);
			location.reload(); //перезагрузить старницу (для ajax не нужно)
				
		}

	});
	//Open sidebar
	$('#open-sidebar').click(function () {
		$('.sidebar-wrapper').toggleClass('active');
		return false;
	});

});

//Delete product item
function deleteProductCart(i){
	var quantity = jQuery.cookie('quantity');
	quantity--;
	jQuery.cookie('quantity', quantity, {
		path: '/'
	});
	jQuery('.quantity').text( jQuery.cookie('quantity') );

	if ( quantity == 0 ) {
		jQuery('.product-basket').html('<li id="no-products">Нет товаров</li>');
		jQuery.cookie('products', jQuery('.product-basket').html() ,{
			path: '/'
		})
	}

	jQuery(i).parent().remove();
	jQuery.cookie( 'products', jQuery('.product-basket').html(), {
		path: '/'
	});
	return false;
};

//YANDEX MAP
ymaps.ready(function () {
	var myMap = new ymaps.Map('ya-map', {
				center: [53.228360, 44.997865],
				zoom: 15
			}, {
				searchControlProvider: 'yandex#search'
			}),

			// Создаём макет содержимого.
			MyIconContentLayout = ymaps.templateLayoutFactory.createClass(
				'<div style="color: #FFFFFF; font-weight: bold;">$[properties.iconContent]</div>'
			),

			myPlacemark = new ymaps.Placemark([53.229123, 45.011282], {
				hintContent: 'г. Пенза ул. Аустрина, 35',
				balloonContent: 'Любимый дом - сеть строительных баз'
			}, {
				iconLayout: 'default#image',
				iconImageHref: '/wp-content/themes/lubim-dom/img/map-pin-silhouette.png',
				iconImageSize: [51, 67],
				iconImageOffset: [-25.5, -25.5]
			})

	myMap.geoObjects
		.add(myPlacemark);

	myMap.behaviors.disable('scrollZoom');
});